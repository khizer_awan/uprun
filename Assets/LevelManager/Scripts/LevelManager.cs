﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Runtime.CompilerServices;
using System.ComponentModel.Design;
using System.Diagnostics;

public class LevelManager : MonoBehaviour {

	[System.Serializable]
	public class level
	{
		public string levelText;
		public int Unlocked;
		public bool isInteractable;
		public Sprite levelThemeSkin;
		public int scoreToUnlock;
	}

	public GameObject[] ThemeList;
	public Transform[] SpacerArray;
	public List<level> LevelList;
	public GameObject LevelButton;
	public int buttonCountEachSpacer;
	[Header("ANIMATORS")]
	public Animator LevelSelectionMenuAnimator;
	public Animator MainMenuPanel;
	public Animator GamePlayPanel;



	private GameObject[] levelButtons;
	private GameObject currentLoadedTheme;
	// Use this for initialization
	void Start () {

//		PlayerPrefs.DeleteAll ();
		FillList (); 
	}
		
	public void FillList(){
		int count = 1;
		int spacerCount=0;
		foreach(var level in LevelList){

			// =========================== INITIALIZING BUTTONS =================================//
			GameObject newButton = Instantiate (LevelButton) as GameObject;
			LevelButton button = newButton.GetComponent<LevelButton> ();
			button.LevelText.text = level.levelText;
			newButton.GetComponent<Image> ().sprite = level.levelThemeSkin;
			newButton.transform.SetParent (SpacerArray [spacerCount],false);


			count++;
			if (count > buttonCountEachSpacer) {	
				count = 1;
				spacerCount++;
			}
//=====================SET LEVEL'S LOCK/UNLOCK STATUS FROM THE PLAYER PREF ==========
			if (PlayerPrefs.GetInt (Commons.KEY_BEST_SCORE, 0) >= level.scoreToUnlock){
				level.Unlocked = 1;
				level.isInteractable = true;
			}
//===============================================================================
			button.unlocked = level.Unlocked;
			button.GetComponent<Button>().interactable = level.isInteractable;
			button.GetComponent<Button> ().onClick.AddListener (() => LoadLevel(LevelList.IndexOf(level)));
			if (!level.isInteractable) {
				button.LockPanel.SetActive (true);
//				button.LevelText.text = "";
				button.scoreToUnlock.text = "Score "+level.scoreToUnlock+"\n" +
					"To Unlock";
			} else {
				button.LockPanel.SetActive (false);
			}

			for (int i = 0; i < PlayerPrefs.GetInt("Level"+button.LevelText.text+"_stars"); i++) {
				button.stars[i].SetActive(true);
			}

		}
			
		SaveAll();

	}

	void SaveAll(){
		
		levelButtons = GameObject.FindGameObjectsWithTag ("levelButtons");
		foreach(GameObject buttonObj in levelButtons){
			LevelButton button = buttonObj.GetComponent<LevelButton> ();
			PlayerPrefs.SetInt ("Level"+button.LevelText, button.unlocked);
			}
 
	}



	void LoadLevel(int LevelIndex){
		GameManager.instance.SetUpGame ();
//		LevelSelectionMenuAnimator.SetTrigger (Commons.KEY_SLIDE_OUT);
//		GamePlayPanel.SetTrigger (Commons.KEY_SLIDE_IN);
		MainMenuPanel.SetTrigger (Commons.KEY_SLIDE_OUT);

		DestroyImmediate (currentLoadedTheme);
		currentLoadedTheme = Instantiate( ThemeList [LevelIndex],Vector3.zero, Quaternion.identity);

	}
		

	public void showMenu(){
//		LevelSelectionMenuAnimator.SetTrigger (Commons.KEY_SLIDE_IN);
		MainMenuPanel.SetTrigger (Commons.KEY_SLIDE_IN);
//		GamePlayPanel.SetTrigger (Commons.KEY_SLIDE_OUT);


	}

}
