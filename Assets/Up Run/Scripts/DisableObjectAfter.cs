﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableObjectAfter : MonoBehaviour {

	public float DisableAfterT=1;
 

	void OnEnable(){
		Invoke ("DisableAfter",DisableAfterT);

	}

	void DisableAfter(){

		gameObject.SetActive (false);


	}
}
