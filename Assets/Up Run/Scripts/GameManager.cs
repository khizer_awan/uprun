﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.SocialPlatforms.Impl;

public class GameManager : MonoBehaviour {

 
	[Header("ANIMATORS")]
	public Animator GamePlayPanel;

	[Header("UI COMPONENTS")]
	public Text ScoreUI;
	public Text BulletsUI;
	public GameObject StartButton;
	public Text BestScoreUI;


	[Header("VARIABLES")]
	public int LevelChangeAfter=20;


	[Header("GAME COMPONENTS")]
	public Tile_Spawner spawner;


	[Header("EXTRAS")]
	public GameObject dummyTile;
	public Transform playerSpawnPoint;



	[Header("EFFECTS")]
	public GameObject TileBurstEffect;
	public GameObject PlayerSpawnEffect;


	[HideInInspector]
	float SurvivalTime;
	public int bullets;

	private mPlayerController player;
	[HideInInspector]
	public int Level;



	public enum GameMode{
		PLAYING,
		GAMEOVER,
		MENU

	}

	 public GameMode currentMode;

	public static  GameManager instance;

 

	//======================== EFFECTS VARIABLES
	private GameObject[] TileBurstEffectPool = new GameObject[3];
	int TilBursteffectIndex ;


	void Awake()
	{


		//If we don't currently have a game control...
		if (instance == null)
			//...set this one to be it...
			instance = this;
		//...otherwise...
		else if(instance != this)
			//...destroy this one because it is a duplicate.
			Destroy (gameObject);


		player = FindObjectOfType<mPlayerController> ();
		spawner = FindObjectOfType<Tile_Spawner>();

 
	}
		


	void Start () {
		GamePlayPanel.SetTrigger (Commons.KEY_SLIDE_OUT);

		InitComponents ();
		
		for(int i = 0 ; i<TileBurstEffectPool.Length; i++){
			TileBurstEffectPool[i] = Instantiate (TileBurstEffect, transform);
			TileBurstEffectPool[i].SetActive (false);
		}

	}


	// Update is called once per frame
	void Update () {
		if (currentMode == GameMode.PLAYING) {
			ScoreUI.text = "" + Mathf.CeilToInt (SurvivalTime);
			SurvivalTime += Time.deltaTime;

		}

	}
		 

	public void SetUpGame(){
		InitComponents ();
		StartButton.SetActive (true);
		GamePlayPanel.SetTrigger (Commons.KEY_RESTART);
		Vector3 spawnLocation = playerSpawnPoint.position;
		Instantiate (PlayerSpawnEffect, spawnLocation, Quaternion.identity);
		player.transform.position = spawnLocation;
		player.gameObject.SetActive (true);
		spawner.ResetTiles ();
		dummyTile.SetActive (true);
	}


	public void StartGame(){
		StartButton.SetActive (false);
		player.flyRight ();
		DestroyTileWithEffect (dummyTile,false);
		currentMode = GameMode.PLAYING;
		spawner.StartSpawning ();
		StartCoroutine (LevelUpRoutine());
	}

	public void GameOver(){
		
		currentMode = GameMode.GAMEOVER;
		GamePlayPanel.SetTrigger (Commons.KEY_GAMEOVER);
		int score = Mathf.CeilToInt (SurvivalTime) + (bullets * 10);
		if (PlayerPrefs.GetInt (Commons.KEY_BEST_SCORE, 0) < score) {
			PlayerPrefs.SetInt (Commons.KEY_BEST_SCORE, score);
		}
		BestScoreUI.text = ""+score;
		StopAllCoroutines ();

	}



	IEnumerator LevelUpRoutine(){ 
		yield return new WaitForSeconds (LevelChangeAfter);
		while(currentMode == GameMode.PLAYING){
			Level++;
			spawner.PowerUpTiles (Level);
			yield return new WaitForSeconds (LevelChangeAfter);

		}
	}
		


	//============================= SIMPLE MATHODS ============================//
	public void DestroyTileWithEffect(GameObject obj,bool killedByBullet){

		obj.SetActive (false);
		if (!killedByBullet) {
			bullets++;
			BulletsUI.text = "" + bullets;
		}
		TilBursteffectIndex++;
		if (TilBursteffectIndex == TileBurstEffectPool.Length) {
			TilBursteffectIndex = 0;
		}
		GameObject burstEffect = TileBurstEffectPool[TilBursteffectIndex];
		burstEffect.transform.position = obj.transform.position;
		burstEffect.SetActive (true);
		StartCoroutine(disableObjectAfter(burstEffect,1f));

	}
		
	IEnumerator disableObjectAfter(GameObject obj,float time){

		yield return new WaitForSeconds (time);

		obj.SetActive (false);

	}
 
		
	void InitComponents(){
		currentMode = GameMode.MENU;
		SurvivalTime = 0;
		ScoreUI.text = "" + Mathf.CeilToInt (SurvivalTime);
		//Tiles broke 1 at start because i am using the same method to destroy the main menu static tile as well
		bullets = 0;
		BulletsUI.text = ""+bullets;
		Level = 0;
		TilBursteffectIndex = 0;
		BestScoreUI.text = ""+PlayerPrefs.GetInt (Commons.KEY_BEST_SCORE,0);


	}
 

}
