﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyObjectAfter : MonoBehaviour {


	public float DestoyAfterT;

	// Use this for initialization
	void Start () {
		Destroy (this.gameObject, DestoyAfterT);
	}
	 
}
