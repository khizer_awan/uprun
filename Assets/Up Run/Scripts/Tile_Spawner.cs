﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine; 

public class Tile_Spawner : MonoBehaviour {

	public GameObject hurdle;
	public int poolSize;
	public float hurdlePause;


	private GameObject[] TilePool;


	int currentHurdleIndex;


	// Use this for initialization
	void Start () {
		
		TilePool = new GameObject[poolSize];
		for(int i=0; i<poolSize; i++){
			TilePool[i] = (GameObject) Instantiate(hurdle,transform);
			TilePool [i].SetActive (false);
		}
 
	}


	void OnEnable(){
		currentHurdleIndex = 0;
		StartSpawning ();
	}

	public void StartSpawning()
	{
		StartCoroutine (HurdleSpawningRoutine());
	}
		

	IEnumerator HurdleSpawningRoutine(){
		yield return new WaitForSeconds (0f);
		while(GameManager.instance.currentMode == GameManager.GameMode.PLAYING){
			spawnHurdle ();
			yield return new WaitForSeconds (hurdlePause);
		}

	}

	public void spawnHurdle(){
		GameObject currentHurdle = TilePool [currentHurdleIndex];
		currentHurdle.SetActive (true);
		currentHurdleIndex++;
		if (currentHurdleIndex == poolSize) {
			currentHurdleIndex = 0;
		}
	}

	public void PowerUpTiles(int Level){

		foreach(GameObject tile in TilePool){
			Tile_Behavior tileBehavior=  tile.GetComponent<Tile_Behavior> (); 
			tileBehavior.currentVelocity += 0.07f;
			tileBehavior.currentMaxPower += Level;

		}
	}


	public void ResetTiles(){
		
		foreach(GameObject tile in TilePool){
			Tile_Behavior tileBehavior=  tile.GetComponent<Tile_Behavior> ();
			tileBehavior.currentMaxPower = tileBehavior.maxPower;
			tileBehavior.currentVelocity = tileBehavior.velocity; 
			tile.SetActive (false);
		}


	}



}
