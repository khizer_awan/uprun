﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Commons : MonoBehaviour {


	//////////////////////////////////////////////// ANIMATION KEYS  ////////////////////////////////////////////////////
	public static string KEY_SLIDE_IN = "slidein";
	public static string KEY_SLIDE_OUT = "slideout";
	public static string KEY_GAMEOVER = "gameover";
	public static string KEY_RESTART = "restart";

	//////////////////////////////////////////////// TAGS ////////////////////////////////////////////////////
	public static string TAG_TILE_DESTROYER = "Destroyer";
	public static string TAG_PLAYER_DESTROYER = "DestroyerPlayer";


	//////////////////////////////////////////////// PREF KEYS ////////////////////////////////////////////////////
	public static string KEY_BEST_SCORE = "bestscore";

}
