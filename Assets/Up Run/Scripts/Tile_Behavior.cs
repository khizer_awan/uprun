﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tile_Behavior : MonoBehaviour {

	public int Power = 0;

	public Text powerUI;

	public float velocity;

	public Color ColorMinPower;
	public Color ColorMaxPower;

	[Header("SIZE")]
	public float minSize = 1f;
	public float maxSize =2f;

	[Header("POWER")]
	public int minPower = 3;
	public int maxPower =7;


	SpriteRenderer sprite;
	Rigidbody2D body;

//	[HideInInspector]
	public int currentMaxPower;

//		[HideInInspector]
	public  float currentVelocity;


	void Awake(){
		
		body = GetComponent<Rigidbody2D> ();
		sprite = GetComponent<SpriteRenderer> ();
 
		currentMaxPower = maxPower; 
		currentVelocity = velocity; 
	}

	private float minX, maxX;
	private float TileSize;

	void OnEnable() {
		
		
		Vector3 screenPosition = Camera.main.ScreenToWorldPoint(new Vector3(Random.Range(0,Screen.width), 1f, Camera.main.farClipPlane/2));
		if (transform.parent) {
			transform.position = new Vector3 (screenPosition.x, transform.parent.position.y, 0);
		}
		float size = Random.Range (minSize, maxSize);
		transform.localScale = new Vector2 (size, 1+(size/5));
		body.velocity = new Vector2 (0f,-currentVelocity);

		Power = Random.Range (minPower,currentMaxPower);
		powerUI.text = "" + Power;
		powerUI.transform.localScale = new Vector3(powerUI.transform.localScale.y, powerUI.transform.localScale.y,1f); 
		sprite.color = Color.Lerp (ColorMinPower,ColorMaxPower, (float) ((Power*100) / maxPower));

		///==================TEST CODE=========================////
		AdjustTileInCameraView (); 

	}

 
	// Update is called once per frame
	void Update () {
		sprite.color = Color.Lerp (ColorMinPower,ColorMaxPower, (float) ((Power*100) / 30)/100);
	}
		
	void OnCollisionEnter2D(Collision2D other) {

		if (other.gameObject.GetComponent<mPlayerController> () != null) {
			Power--;
			powerUI.text = "" + Power;
			if (Power == 0) {
				GameManager.instance.DestroyTileWithEffect (gameObject,false);
			}
		}  
	}

	public void OnTriggerEnter2D(Collider2D other){
		
		if (other.tag.Equals (Commons.TAG_TILE_DESTROYER)) {
			gameObject.SetActive (false);

		}

	}

	void AdjustTileInCameraView(){


		///==================TEST CODE=========================////
		/// 
		// If you want the min max values to update if the resolution changes 
		// set them in update else set them in Start
		float camDistance = Vector3.Distance(transform.position, Camera.main.transform.position);
		Vector2 bottomCorner = Camera.main.ViewportToWorldPoint(new Vector3(0,0, camDistance));
		Vector2 topCorner = Camera.main.ViewportToWorldPoint(new Vector3(1,1, camDistance));

		//in Start
		BoxCollider2D playerCollider = GetComponent<BoxCollider2D>();
		TileSize = playerCollider.bounds.extents.x;

		//replace your code with this
		minX = bottomCorner.x + TileSize;
		maxX = topCorner.x - TileSize;

		// Get current position
		Vector3 pos = transform.position;

		// Horizontal contraint
		//left

		if (pos.x < minX) 
		pos.x = minX ;//+ (minX * GameManager.instance.displacementPercentageLeft / 100);

//		}
		//right
		if (pos.x > maxX)
		pos.x = maxX;//-(maxX*GameManager.instance.displacementPercentageRight/100);


		// Update position
		transform.position = pos;


	}


}
