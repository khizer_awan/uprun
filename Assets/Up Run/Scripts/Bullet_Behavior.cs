﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet_Behavior : MonoBehaviour {


	public float disableAfter;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
		

	void OnEnable(){

//		StartCoroutine (disable());

	}

	public void OnTriggerEnter2D(Collider2D other){

		if (other.GetComponent<Tile_Behavior> ()) {
			GameManager.instance.DestroyTileWithEffect (other.gameObject,true);
			gameObject.SetActive (false);
		} else if (other.tag.Equals (Commons.TAG_TILE_DESTROYER)) {
			gameObject.SetActive (false);
		}

	}

	IEnumerator disable(){
	
		yield return new WaitForSeconds (disableAfter);

		gameObject.SetActive (false);
	
	}

}
