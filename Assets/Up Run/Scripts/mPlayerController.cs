﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using NUnit.Framework.Internal;

public class mPlayerController : MonoBehaviour {


	Rigidbody2D body;

	[Header("Player Settings")]
	public float flySpeedVertical = 10f;
	public float flySpeedHorrizontal; 

	[Header("Bullet Settings")]
	public GameObject BulletPrefab;
	public float bulletForce;
	private GameObject[] BulletsPool;
	int currentBulletIndex;

 


	[Header("FURR BALL STUFF")] 
	public Transform groundCheck;
	public bool isGrounded = false;
	public LayerMask whatIsGround;
	public float radiusOfGroundCheck;
	//====================================

	private bool lookingRight=true;
	private Animator anim;


	//==================================== HEAD HIT EFFECT =================================================
	[Header("HEADHIT STUFF")]
	public GameObject HitHeadEffectPrefab;
	public int HitHeadPoolSize=3;
	private GameObject[] HitHeadPool;
	private int currentHitHeadIndex;
	public int SpeedHitEffectAppear;
 

	[Header("PARTICLE EFFECTS")]
	public GameObject DeathEffect;





	//=======================TEMP
	private float minX, maxX;
	private float minY, maxY;
	private float TileSize;
	private float playerRadius;





	void Awake(){

		body = GetComponent<Rigidbody2D> ();


	}


	// Use this for initialization
	void Start () { 
		currentHitHeadIndex = 0;
		currentBulletIndex = 0;


		anim = GetComponent<Animator>();


		//=================POPULATING BULLETS POOL =======================//
		BulletsPool = new GameObject[HitHeadPoolSize];
		for(int i =0; i< BulletsPool.Length ; i++){

			BulletsPool[i] = Instantiate (BulletPrefab, Vector3.zero,Quaternion.identity);
			BulletsPool [i].SetActive (false);

		}
 
		//=================POPULATING HitHeadEffect POOL =======================//
		HitHeadPool = new GameObject[HitHeadPoolSize];
		for(int i =0; i< HitHeadPool.Length ; i++){
			HitHeadPool[i] = Instantiate (HitHeadEffectPrefab, Vector3.zero,Quaternion.identity);
			HitHeadPool [i].SetActive (false);

		}


		CalculatingBoundaries ();
	}
	
	// Update is called once per frame
	void Update () {
 
		#if UNITY_EDITOR
		if (GameManager.instance != null) {
			if (GameManager.instance.currentMode == GameManager.GameMode.PLAYING) {
				if (Input.GetKeyDown (KeyCode.V)) {
					flyRight ();
				}
				if (Input.GetKeyDown (KeyCode.C)) {
					flyLeft ();
				}
				if (Input.GetKeyDown (KeyCode.X)) {
					Shoot ();
				}
			}
		}
		//===========Test code

		if (Input.GetKeyDown (KeyCode.Space)) {
			body.velocity = Vector2.zero;
			Vector2 flyForce = new Vector2 (0f, flySpeedVertical);
			body.AddForce (flyForce);
		}

		//===============
		#endif


	}
		
		

	public void flyRight(){
//		if (!isFlyPaused) {
			body.velocity = Vector2.zero;
//			Vector2 flyForce = new Vector2 (flySpeedHorrizontal, flySpeedVertical);
		body.AddForce (new Vector2 (flySpeedHorrizontal, flySpeedVertical));
//		}
	}

	public void flyLeft(){
		
//		if (!isFlyPaused) {
			body.velocity = Vector2.zero;
//			Vector2 flyForce = new Vector2 (-flySpeedHorrizontal, flySpeedVertical);
		body.AddForce (new Vector2 (-flySpeedHorrizontal, flySpeedVertical));

//		}

	}


	public void Shoot(){
		if (GameManager.instance.bullets > 0) {
			//========================= PUSHING PLAYER UP (JUST TO SUPPORT PLAYER)===========================//
			body.velocity = Vector2.zero;
			Vector2 flyForce = new Vector2 (0f, flySpeedVertical);
			body.AddForce (flyForce);
			//=================


			//============================ SHOOTING A BULLET
			currentBulletIndex++;
			if (currentBulletIndex == BulletsPool.Length) {
				currentBulletIndex = 0;
			}


			GameManager.instance.bullets--;
			GameManager.instance.BulletsUI.text = GameManager.instance.bullets+"";

			GameObject bullet = BulletsPool [currentBulletIndex];
			bullet.transform.position = transform.position;
			bullet.SetActive (true);
			bullet.GetComponent<Rigidbody2D> ().velocity = Vector2.zero;
			bullet.GetComponent<Rigidbody2D> ().AddForce (new Vector2 (0f, bulletForce));

//			StartCoroutine (disableObjectAfter (bullet,3f));
		}
	}


	public void OnTriggerEnter2D(Collider2D other){
		if (other.tag.Equals (Commons.TAG_PLAYER_DESTROYER)) { 
			GameManager.instance.GameOver ();
			Instantiate (DeathEffect, transform.position, Quaternion.identity);
			gameObject.SetActive (false);
		}
	}

 
	//========================== MOSTLY FURR BALL STUFF ============================
	void OnCollisionEnter2D(Collision2D collision2D) {
		if (collision2D.relativeVelocity.magnitude > SpeedHitEffectAppear){
			showHitHeadEffect (collision2D.contacts[0].point);
		}
	}
	void FixedUpdate(){
 

		KeepingBallInScreen ();

		//==========DETERMINING THE DIRECTION OF THE SPEED TO CALCULATE THE FACE DIRECTION OF THE BALL ================
		if (body.velocity.x > 0) {
			if (!lookingRight) {
				Flip ();
				lookingRight = true;
			}
		} else if (body.velocity.x < 0) {
			if (lookingRight) {
				Flip ();
				lookingRight = false;
			}
		} else {
		}

		isGrounded = Physics2D.Raycast (groundCheck.position,-transform.up,0.05f);//OverlapCircle (groundCheck.position, radiusOfGroundCheck);
			//======= flurry's flurr effect
		anim.SetBool ("IsGrounded", isGrounded);
		anim.SetFloat ("vSpeed", body.velocity.y);
		//=======

	}


	public void Flip()
	{
		Vector3 myScale = transform.localScale;
		myScale.x *= -1;
		transform.localScale = myScale;
	}

 

	void showHitHeadEffect(Vector2 pos){
		currentHitHeadIndex++;
		if (currentHitHeadIndex == HitHeadPool.Length) {
			currentHitHeadIndex = 0;
		}

		GameObject hitHead = HitHeadPool [currentHitHeadIndex];
		hitHead.SetActive (true);
		hitHead.transform.position = pos; 

	}




	//========================== LIMITING PLAYER IN THE SCREEN BOUNDARY ============================

	void CalculatingBoundaries(){
		
		///==================TEST CODE=========================////
		// If you want the min max values to update if the resolution changes 
		// set them in update else set them in Start
		float camDistance = Vector3.Distance(transform.position, Camera.main.transform.position);
		Vector2 bottomCorner = Camera.main.ViewportToWorldPoint(new Vector3(0,0, camDistance));
		Vector2 topCorner = Camera.main.ViewportToWorldPoint(new Vector3(1,1, camDistance));
		
		
		//in Start
		CircleCollider2D playerCollider = GetComponent<CircleCollider2D>();
		playerRadius = playerCollider.bounds.extents.x;
		
		//replace your code with this
		minX = bottomCorner.x + playerRadius;
		maxX = topCorner.x - playerRadius;
		//		minY = bottomCorner.y + playerRadius;
		maxY = topCorner.y - playerRadius;
		
		
		
	}
	
	private Vector3 pos;
	void KeepingBallInScreen(){
		
		// Get current position
		pos = transform.position;
		
		// Horizontal contraint
		if (pos.x < minX) {
			pos.x = minX;
			body.velocity= new Vector2(-body.velocity.x/2, body.velocity.y);
		}
		if (pos.x > maxX) {
			pos.x = maxX;
			body.velocity= new Vector2(-body.velocity.x/2, body.velocity.y);
		}
		
		// vertical contraint
		if (pos.y > maxY) {
			pos.y = maxY;
			body.velocity = new Vector2(body.velocity.x, -body.velocity.y/2);
		}
		
		// Update position
		transform.position = pos;
		
	}
	

}
